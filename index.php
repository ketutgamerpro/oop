<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Nama hewan adalah: $sheep->name <br>"; // "shaun"
echo "Jumlah kaki yang dimiliki adalah: $sheep->legs <br>"; // 2
echo "Apakah dia berdarah dingin: $sheep->cold_blooded"; // false

$sungokong = new Ape("kera sakti");

echo "<br><br>Nama hewan adalah: $sungokong->name <br>"; 
echo "Jumlah kaki yang dimiliki adalah: $sungokong->legs <br>"; 
echo "Apakah dia berdarah dingin: $sungokong->cold_blooded <br>"; 
echo "Suara yang dikeluarkan: ".$sungokong->yell(); // "Auooo"

$kodok = new Frog("Buduk");

echo "<br><br>Nama hewan adalah: $kodok->name <br>"; 
echo "Jumlah kaki yang dimiliki adalah: $kodok->legs <br>"; 
echo "Apakah dia berdarah dingin: $kodok->cold_blooded <br>"; 
echo "Katak suka melompat : ".$kodok->jump();// "hop hop"


?>